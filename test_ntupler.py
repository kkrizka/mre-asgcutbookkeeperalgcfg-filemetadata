#!/usr/bin/env python

import os

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

# Configure input
from AthenaConfiguration.AllConfigFlags import ConfigFlags
ConfigFlags.Input.Files = [os.environ['ASG_TEST_FILE_MC']]

ConfigFlags.lock()

# Tools for reading stuff
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
acc = MainServicesCfg(ConfigFlags)

from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg
acc.merge(MetaDataSvcCfg(ConfigFlags))

from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
acc.merge(PoolReadCfg(ConfigFlags))

#####
def SystematicsSvcCfg(flags, name='SystematicsSvc', **kwargs):
    acc = ComponentAccumulator()

    kwargs.setdefault('sigmaRecommended', 1)
    acc.addService(CompFactory.CP.SystematicsSvc(name,**kwargs))

    return acc

def AsgCutBookkeeperAlgCfg(flags, name='AsgCutBookkeeper', **kwargs):
    acc = ComponentAccumulator()

    kwargs.setdefault('runNumber', ConfigFlags.Input.RunNumber[0])
    acc.addEventAlgo(CompFactory.CP.AsgCutBookkeeperAlg(name, **kwargs))

    return acc

def TreeMakerAlgCfg(flags, name='TreeMaker', **kwargs):
    acc = ComponentAccumulator()

    acc.addEventAlgo(CompFactory.CP.TreeMakerAlg(name, **kwargs))

    return acc

def TreeFillerAlgCfg(flags, name='TreeFiller', **kwargs):
    acc = ComponentAccumulator()

    acc.addEventAlgo(CompFactory.CP.TreeFillerAlg(name, **kwargs))

    return acc

def NTupleMakerEventNumberCfg(flags, name='NTupleMakerEventInfo', **kwargs):
    kwargs.setdefault('Branches', [
        'EventInfo.runNumber -> runNumber',
        'EventInfo.eventNumber -> eventNumber',
    ])

    acc = ComponentAccumulator()

    acc.addEventAlgo(CompFactory.CP.AsgxAODNTupleMakerAlg(name, **kwargs))

    return acc

def testconfig(flags):
    acc = ComponentAccumulator()

    acc.merge(SystematicsSvcCfg(flags))
    acc.merge(AsgCutBookkeeperAlgCfg(flags))
    acc.merge(TreeMakerAlgCfg(flags))
    acc.merge(NTupleMakerEventNumberCfg(flags))
    acc.merge(TreeFillerAlgCfg(flags))

    return acc

acc.merge(testconfig(ConfigFlags))
#####

# finally, set up the infrastructure for writing our output
from GaudiSvc.GaudiSvcConf import THistSvc
histSvc = CompFactory.THistSvc()
histSvc.Output += ["ANALYSIS DATAFILE='output.root' OPT='RECREATE'"]
acc.addService(histSvc)

acc.printConfig(withDetails=True)

# Execute and finish
sc = acc.run(maxEvents=-1)
 
# Success should be 0
import sys
sys.exit(not sc.isSuccess())


