# MRE AsgxAODNTupleMakerAlg Branches

MRE demonstrating a problem with trying to run the AsgCutBookkeeperAlg in a CA-based script. It runs on the `ASG_TEST_FILE_MC` file, initializes a cutflow bookkeeper and saves EventInfo in a TTree.

The observed error is the following:
```
ClassIDSvc                                           INFO getRegistryEntries: read 10125 CLIDRegistry entries for module ALL
InputMetaDataStore                                WARNING retrieve(const): No valid proxy for object FileMetaData  of type xAOD::FileMetaData(CLID 178309087)
AsgCutBookkeeper                                    ERROR /global/homes/k/kkrizka/DiBJetTLA_Organizer/source/athena/PhysicsAnalysis/Algorithms/AsgAnalysisAlgorithms/Root/AsgCutBookkeeperAlg.cxx:59 (virtual StatusCode CP::AsgCutBookkeeperAlg::initialize()): Failed to call "inputMetaStore()->retrieve(fmd, "FileMetaData")"
AsgCutBookkeeper                                    ERROR /build/atnight/localbuilds/nightlies/Athena/22.0/athena/Control/AthenaBaseComps/AthenaBaseComps/AthCommonDataStore.icc:43 (StatusCode AthCommonDataStore<PBASE>::sysInitialize() [with PBASE = AthCommonMsg<Algorithm>]): code FAILURE: PBASE::sysInitialize()
AthAlgSeq                                           ERROR Unable to initialize Algorithm CP::AsgCutBookkeeperAlg/AsgCutBookkeeper
AthAlgSeq                                           ERROR AthenaBaseComps/AthenaBaseComps/AthCommonDataStore.icc:43 (StatusCode AthCommonDataStore<PBASE>::sysInitialize() [with PBASE = AthCommonMsg<Gaudi::Sequence>]): code FAILURE: PBASE::sysInitialize()
AthAllAlgSeq                                        ERROR Unable to initialize Algorithm AthSequencer/AthAlgSeq
AthAllAlgSeq                                        ERROR AthenaBaseComps/AthenaBaseComps/AthCommonDataStore.icc:43 (StatusCode AthCommonDataStore<PBASE>::sysInitialize() [with PBASE = AthCommonMsg<Gaudi::Sequence>]): code FAILURE: PBASE::sysInitialize()
AthAlgEvtSeq                                        ERROR Unable to initialize Algorithm AthSequencer/AthAllAlgSeq
AthAlgEvtSeq                                        ERROR AthenaBaseComps/AthenaBaseComps/AthCommonDataStore.icc:43 (StatusCode AthCommonDataStore<PBASE>::sysInitialize() [with PBASE = AthCommonMsg<Gaudi::Sequence>]): code FAILURE: PBASE::sysInitialize()
AthMasterSeq                                        ERROR Unable to initialize Algorithm AthSequencer/AthAlgEvtSeq
AthMasterSeq                                        ERROR AthenaBaseComps/AthenaBaseComps/AthCommonDataStore.icc:43 (StatusCode AthCommonDataStore<PBASE>::sysInitialize() [with PBASE = AthCommonMsg<Gaudi::Sequence>]): code FAILURE: PBASE::sysInitialize()
AthenaEventLoopMgr                                  ERROR Unable to initialize Algorithm: AthMasterSeq
AthenaEventLoopMgr                                  ERROR Failed to initialize base class MinimalEventLoopMgr
ServiceManager                                      ERROR Unable to initialize Service: AthenaEventLoopMgr
```

Fix is to add the following:
```python
from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg
acc.merge(MetaDataSvcCfg(ConfigFlags))
```

Note that the `AsgCutBookkeeperAlg` does not work with data as the `PMGTools::PMGTruthWeightTool` is always used.